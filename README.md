# Rohlik Case Study

## Requirements

Application requires Java JDK version 17+.

## Usage

    ./mvnw spring-boot:run
   
## Documentation

- [API Documentation](http://localhost:8080/swagger-ui/index.html)
- [Status](http://localhost:8080/actuator/)
