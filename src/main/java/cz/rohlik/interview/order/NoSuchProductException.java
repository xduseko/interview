package cz.rohlik.interview.order;

/**
 * The product cannot be found. It does not exist or has been marked as deleted.
 */
public class NoSuchProductException extends RuntimeException {

    static final long serialVersionUID = 1L;

    public NoSuchProductException(Long productId) {
        super(String.format("The product cannot be found: %s", productId));
    }
}
