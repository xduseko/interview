package cz.rohlik.interview.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.Valid;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "app_order")
@Schema(description = "The sale of products to be delivered")
public class Order {

    @Id
    @GeneratedValue
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Schema(description = "Unique order identifier")
    private Long id;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonView(OrderOperation.Update.class)
    @Schema(description = "Lifecycle phase of the order")
    private OrderState state;

    @Column(nullable = false)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Schema(description = "Order creation date")
    private OffsetDateTime timestamp;

    @ElementCollection
    @Valid
    @JsonView(OrderOperation.Create.class)
    @Schema(description = "Products along with quantities to be delivered")
    private List<OrderItem> items = new ArrayList<>();

    public Order() {
    }

    public Order(OrderState state, OffsetDateTime timestamp, List<OrderItem> items) {
        this.state = state;
        this.timestamp = timestamp;
        this.items = items;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OrderState getState() {
        return state;
    }

    public void setState(OrderState state) {
        this.state = state;
    }

    public OffsetDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(OffsetDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Order{");
        sb.append("id=").append(id);
        sb.append('}');
        return sb.toString();
    }
}
