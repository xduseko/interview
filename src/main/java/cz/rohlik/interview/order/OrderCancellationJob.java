package cz.rohlik.interview.order;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;

/**
 * Job that regularly checks orders and cancels expired ones
 */
@Service
public class OrderCancellationJob {

    private final Logger logger = LoggerFactory.getLogger(OrderCancellationJob.class);

    private final OrderRepository orderRepository;

    private final OrderService orderService;

    private final OrderProperties orderProperties;

    public OrderCancellationJob(OrderRepository orderRepository, OrderService orderService,
            OrderProperties orderProperties) {
        this.orderRepository = orderRepository;
        this.orderService = orderService;
        this.orderProperties = orderProperties;
    }

    @Scheduled(cron = "#{@orderProperties.cancellationJobCron}")
    public void cancelExpiredOrders() {
        cancelExpiredOrders(OffsetDateTime.now());
    }

    public void cancelExpiredOrders(OffsetDateTime now) {
        final OffsetDateTime timestampBefore = now.minusMinutes(orderProperties.getPaymentExpiration());
        final List<Order> orders = orderRepository.findByStateAndTimestampBefore(OrderState.CREATED, timestampBefore);
        logger.debug("Found {} expired orders", orders.size());
        orders.forEach(order -> cancelOrder(order));
    }

    private void cancelOrder(Order order) {
        try {
            logger.debug("Cancelling {}", order);
            orderService.cancel(order.getId());
            logger.info("{} has been cancelled", order);
        } catch (Exception exception) {
            logger.error("Unable to cancel order", exception);
        }
    }
}
