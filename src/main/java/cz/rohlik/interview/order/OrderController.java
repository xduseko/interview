package cz.rohlik.interview.order;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.time.OffsetDateTime;
import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderController {

    private final OrderRepository orderRepository;

    private final OrderService orderService;

    private final OrderCreateService orderCreateService;

    public OrderController(OrderRepository orderRepository, OrderService orderService,
            OrderCreateService orderCreateService) {
        this.orderRepository = orderRepository;
        this.orderService = orderService;
        this.orderCreateService = orderCreateService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Order> list() {
        return orderRepository.findWithItems();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Order create(@RequestBody @JsonView(OrderOperation.Create.class) @Validated(OrderOperation.Create.class)
            Order order) {
        order.setState(OrderState.CREATED);
        order.setTimestamp(OffsetDateTime.now());
        return orderCreateService.create(order);
    }

    @PatchMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Order update(@PathVariable Long id, @RequestBody @JsonView(OrderOperation.Update.class) Order order) {
        final OrderState state = order.getState();
        if (state != null) {
            return updateState(id, state);
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No property to update found");
    }

    private Order updateState(Long id, OrderState state) {
        if (state == OrderState.PAID) {
            return orderService.pay(id);
        }
        if (state == OrderState.CANCELLED) {
            return orderService.cancel(id);
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unsupported order state value");
    }
}
