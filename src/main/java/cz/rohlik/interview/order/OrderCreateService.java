package cz.rohlik.interview.order;

import cz.rohlik.interview.product.Product;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class OrderCreateService {

    private final OrderRepository orderRepository;

    private final OrderService orderService;

    public OrderCreateService(OrderRepository orderRepository, OrderService orderService) {
        this.orderRepository = orderRepository;
        this.orderService = orderService;
    }

    /**
     * Places an order in the database
     * @param order Order to create
     * @return Order created
     * @throws NoSuchProductException For a non-existent or deleted product
     * @throws OutOfStockException For quantities out of stock
     */
    public Order create(Order order) {
        final List<OrderItem> items = order.getItems();
        final Map<Long, Product> productMap = orderService.findProductsForUpdate(items);
        items.forEach(item -> {
            final Long productId = item.getProductId();
            final Product product = productMap.get(productId);
            if (product == null || product.getDeleted()) {
                throw new NoSuchProductException(productId);
            }
            final Long stockQuantity = product.getStockQuantity() - item.getQuantity();
            product.setStockQuantity(stockQuantity);
        });
        validateProducts(productMap.values());
        return orderRepository.save(order);
    }

    private void validateProducts(Iterable<Product> products) {
        final List<OutOfStockItem> items = new ArrayList<>();
        products.forEach(product -> {
            final long stockQuantity = product.getStockQuantity();
            if (stockQuantity < 0) {
                items.add(new OutOfStockItem(product.getId(), -stockQuantity));
            }
        });
        if (!items.isEmpty()) {
            throw new OutOfStockException(items);
        }
    }
}
