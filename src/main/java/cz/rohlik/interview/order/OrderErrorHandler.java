package cz.rohlik.interview.order;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice(assignableTypes = OrderController.class)
public class OrderErrorHandler {

    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ExceptionHandler(TerminalStateException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public OrderErrorResponseEntity handleException(TerminalStateException exception) {
        return new OrderErrorResponseEntity(exception.getMessage());
    }

    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ExceptionHandler(NoSuchProductException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public OrderErrorResponseEntity handleException(NoSuchProductException exception) {
        return new OrderErrorResponseEntity(exception.getMessage());
    }

    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ExceptionHandler(OutOfStockException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public OrderErrorResponseEntity handleException(OutOfStockException exception) {
        return new OrderErrorResponseEntity(exception.getMessage(), exception.getItems());
    }
}
