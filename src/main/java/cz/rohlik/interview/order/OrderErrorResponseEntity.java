package cz.rohlik.interview.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

/**
 * Order end-point error body
 * @param message Error message
 * @param outOfStockItems List of out-of-stock products
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public record OrderErrorResponseEntity(
        @Schema(description = "Error message") String message,
        @Schema(description = "Items that can not be ordered") List<OutOfStockItem> outOfStockItems) {

    OrderErrorResponseEntity(String message) {
        this(message, null);
    }
}
