package cz.rohlik.interview.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import cz.rohlik.interview.product.Product;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Embeddable
@Schema(description = "Product with quantity to be delivered")
public class OrderItem {

    public OrderItem() {
    }

    public OrderItem(Long productId, Long quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "product_id", nullable = false, insertable = false, updatable = false)
    @JsonIgnore
    private Product product;

    @Column(name = "product_id")
    @NotNull(groups = OrderOperation.Create.class)
    @JsonView(OrderOperation.Create.class)
    @Schema(description = "Product identifier")
    private Long productId;

    @Column(nullable = false)
    @NotNull(groups = OrderOperation.Create.class)
    @Min(groups = OrderOperation.Create.class, value = 1)
    @JsonView(OrderOperation.Create.class)
    @Schema(description = "Quantity to be delivered")
    private Long quantity;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
