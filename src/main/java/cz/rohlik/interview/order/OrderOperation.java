package cz.rohlik.interview.order;

public interface OrderOperation {
    interface Create {}
    interface Update {}
}
