package cz.rohlik.interview.order;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "order")
public class OrderProperties {

    /**
     * Remaining minutes to make a payment before created order gets cancelled
     */
    private Long paymentExpiration = 30L;

    /**
     * Cron expression for order cancellation job
     */
    private String cancellationJobCron = "1 * * * * ?";

    public Long getPaymentExpiration() {
        return paymentExpiration;
    }

    public void setPaymentExpiration(Long paymentExpiration) {
        this.paymentExpiration = paymentExpiration;
    }

    public String getCancellationJobCron() {
        return cancellationJobCron;
    }

    public void setCancellationJobCron(String cancellationJobCron) {
        this.cancellationJobCron = cancellationJobCron;
    }
}
