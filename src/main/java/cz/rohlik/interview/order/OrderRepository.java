package cz.rohlik.interview.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.time.OffsetDateTime;
import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Order findForUpdateById(Long id);

    @Query("select distinct o from Order o join fetch o.items")
    List<Order> findWithItems();

    List<Order> findByStateAndTimestampBefore(OrderState state, OffsetDateTime timestampBefore);
}
