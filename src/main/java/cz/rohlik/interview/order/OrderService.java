package cz.rohlik.interview.order;

import cz.rohlik.interview.product.Product;
import cz.rohlik.interview.product.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class OrderService {

    private final OrderRepository orderRepository;

    private final ProductRepository productRepository;

    public OrderService(OrderRepository orderRepository, ProductRepository productRepository) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
    }

    /**
     * Cancels the order and releases its items to stock
     * @param id Order identifier
     * @return Order cancelled
     * @throws TerminalStateException For order in terminal state
     * @throws NoSuchProductException For a non-existent product
     */
    public Order cancel(Long id) {
        final Order order = updateState(id, OrderState.CANCELLED);
        final List<OrderItem> items = order.getItems();
        final Map<Long, Product> productMap = findProductsForUpdate(items);
        items.forEach(item -> {
            final Long productId = item.getProductId();
            final Product product = productMap.get(productId);
            if (product == null) {
                throw new NoSuchProductException(productId);
            }
            final Long stockQuantity = product.getStockQuantity() + item.getQuantity();
            product.setStockQuantity(stockQuantity);
        });
        return order;
    }

    /**
     * Marks the order as paid
     * @param id Order identifier
     * @return Order paid
     * @throws TerminalStateException For order in terminal state
     */
    public Order pay(Long id) {
        return updateState(id, OrderState.PAID);
    }

    /**
     * Finds and locks products in order items
     * @param items Order items
     * @return Map of found products, where the key is the product identifier
     */
    public Map<Long, Product> findProductsForUpdate(List<OrderItem> items) {
        final Set<Long> productIds = items.stream().map(item -> item.getProductId()).collect(Collectors.toSet());
        final List<Product> products = productRepository.findForUpdateByIdIn(productIds);
        return products.stream().collect(Collectors.toMap(product -> product.getId(), product -> product));
    }

    private Order updateState(Long id, OrderState state) {
        final Order order = orderRepository.findForUpdateById(id);
        if (order.getState().isTerminal()) {
            throw new TerminalStateException(id);
        }
        order.setState(state);
        return order;
    }
}
