package cz.rohlik.interview.order;

public enum OrderState {

    /**
     * Order is ready to be delivered after the payment is received
     */
    CREATED(false),

    /**
     * Order has been successfully paid
     */
    PAID(true),

    /**
     * Order has been cancelled and can no longer be fulfilled
     */
    CANCELLED(true);

    /**
     * Order state can not be updated from this value
     */
    private final boolean terminal;

    OrderState(boolean terminal) {
        this.terminal = terminal;
    }

    public boolean isTerminal() {
        return terminal;
    }
}
