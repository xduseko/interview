package cz.rohlik.interview.order;

import java.util.List;

/**
 * Product out-of-stock
 */
public class OutOfStockException extends RuntimeException {

    static final long serialVersionUID = 1L;

    /**
     * List of out-of-stock products
     */
    private final List<OutOfStockItem> items;

    public OutOfStockException(List<OutOfStockItem> items) {
        super("Product out-of-stock");
        this.items = List.copyOf(items);
    }

    public List<OutOfStockItem> getItems() {
        return items;
    }
}
