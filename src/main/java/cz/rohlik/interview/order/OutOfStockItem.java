package cz.rohlik.interview.order;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * Detailed information about the product that is out of stock
 * @param productId Product identifier
 * @param quantityMissing Quantity needed to fulfill the order
 */
public record OutOfStockItem(
        @Schema(description = "Product identifier") Long productId,
        @Schema(description = "Quantity needed to fulfill the order") Long quantityMissing) implements Serializable {

    static final long serialVersionUID = 1L;
}
