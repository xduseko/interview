package cz.rohlik.interview.order;

/**
 * The state of the order is terminal and cannot be changed
 */
public class TerminalStateException extends RuntimeException {

    static final long serialVersionUID = 1L;

    public TerminalStateException(Long orderId) {
        super(String.format("The state of the order is terminal and cannot be changed: %s", orderId));
    }
}
