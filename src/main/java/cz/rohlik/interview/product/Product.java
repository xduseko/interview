package cz.rohlik.interview.product;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Schema(description = "Type of goods offered for sale")
public class Product {

    @Id
    @GeneratedValue
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Schema(description = "Unique product identifier")
    private Long id;

    @Column(nullable = false)
    @NotNull
    @Schema(description = "Name of the product")
    private String name;

    @Column(nullable = false)
    @NotNull
    @Min(value = 0)
    @Schema(description = "The price for a single unit of a product")
    private Long unitPrice;

    @Column(nullable = false)
    @NotNull
    @Min(value = 0)
    @Schema(description = "Unit count of a product available")
    private Long stockQuantity;

    @Column(nullable = false)
    @JsonIgnore
    private Boolean deleted = false;

    public Product() {
    }

    public Product(String name, Long unitPrice, Long stockQuantity, Boolean deleted) {
        this.name = name;
        this.unitPrice = unitPrice;
        this.stockQuantity = stockQuantity;
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Long unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Long getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(Long stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
