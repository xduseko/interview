package cz.rohlik.interview.product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Set;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> findAllByDeletedFalse();

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Product findForUpdateById(Long id);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    List<Product> findForUpdateByIdIn(Set<Long> id);
}
