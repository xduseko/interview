package cz.rohlik.interview.product;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /**
     * Puts the product in the database
     * @param product Product to create
     * @return Product created
     */
    public Product create(Product product) {
        return productRepository.save(product);
    }

    /**
     * Updates the product in the database
     * @param id Product identifier
     * @param updatedProduct Product to update
     * @return Product updated
     */
    public Product update(Long id, Product updatedProduct) {
        final Product product = productRepository.findForUpdateById(id);
        product.setName(updatedProduct.getName());
        product.setUnitPrice(updatedProduct.getUnitPrice());
        product.setStockQuantity(updatedProduct.getStockQuantity());
        return product;
    }

    /**
     * Marks the product in the database as deleted
     * @param id Product identifier
     */
    public void delete(Long id) {
        final Product product = productRepository.findForUpdateById(id);
        product.setDeleted(true);
    }
}
