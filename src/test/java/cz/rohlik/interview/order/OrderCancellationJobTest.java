package cz.rohlik.interview.order;

import cz.rohlik.interview.IntegrationTest;
import cz.rohlik.interview.product.ProductTestService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.test.annotation.DirtiesContext;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;

@IntegrationTest
@AutoConfigureTestDatabase
class OrderCancellationJobTest {

    @Autowired
    private ProductTestService productTestService;

    @Autowired
    private OrderTestService orderTestService;

    @Autowired
    private OrderCancellationJob orderCancellationJob;

    @Test
    @DirtiesContext
    public void testCancelExpiredOrders() {
        final Long milkId = productTestService.createProduct("Milk", 20L, 10L);
        final Long breadId = productTestService.createProduct("Bread", 30L, 20L);

        final OrderItem milkItem = new OrderItem(milkId, 5L);
        final OrderItem breadItem = new OrderItem(breadId, 1L);
        final OffsetDateTime timestamp = OffsetDateTime.of(
                LocalDateTime.of(2020, Month.JANUARY, 1, 10, 0), ZoneOffset.UTC);
        final Long orderId = orderTestService.createOrder(OrderState.CREATED, timestamp, List.of(milkItem, breadItem));

        final LocalDate localDate = LocalDate.of(2020, Month.JANUARY, 1);
        final LocalTime localTime = LocalTime.of(10, 31);
        final OffsetDateTime now = OffsetDateTime.of(localDate, localTime, ZoneOffset.UTC);
        orderCancellationJob.cancelExpiredOrders(now);

        orderTestService.assertOrderState(OrderState.CANCELLED, orderId);
        productTestService.assertStockQuantity(15L, milkId);
        productTestService.assertStockQuantity(21L, breadId);
    }
}
