package cz.rohlik.interview.order;

import cz.rohlik.interview.IntegrationTest;
import cz.rohlik.interview.product.ProductTestService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@IntegrationTest
@AutoConfigureTestDatabase
@AutoConfigureMockMvc
class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ProductTestService productTestService;

    @Autowired
    private OrderTestService orderTestService;

    private Long milkId;

    private Long breadId;

    @BeforeEach
    void prepare() {
        milkId = productTestService.createProduct("Milk", 20L, 10L);
        breadId = productTestService.createProduct("Bread", 40L, 10L);
    }

    @Test
    @DirtiesContext
    void testList() throws Exception {
        final OffsetDateTime timestamp = OffsetDateTime.of(
                LocalDateTime.of(2020, Month.JANUARY, 1, 10, 0), ZoneOffset.UTC);
        final OrderItem milkItem = new OrderItem(milkId, 2L);
        final OrderItem breadItem = new OrderItem(breadId, 1L);
        orderTestService.createOrder(OrderState.CREATED, timestamp, List.of(milkItem, breadItem));
        mockMvc.perform(get("/orders").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].state", is("CREATED")))
                .andExpect(jsonPath("$[0].timestamp", is("2020-01-01T10:00:00Z")))
                .andExpect(jsonPath("$[0].items[0].productId", is(milkId), Long.class))
                .andExpect(jsonPath("$[0].items[0].quantity", is(2L), Long.class))
                .andExpect(jsonPath("$[0].items[1].productId", is(breadId), Long.class))
                .andExpect(jsonPath("$[0].items[1].quantity", is(1L), Long.class));
    }

    @Test
    @DirtiesContext
    void testCreate() throws Exception {
        final String content = String.format("""
                {
                    "items": [
                        {
                            "productId": %d,
                            "quantity": 3
                        },
                        {
                            "productId": %d,
                            "quantity": 4
                        }
                    ]
                }
                """, milkId, breadId);
        mockMvc.perform(post("/orders").contentType(MediaType.APPLICATION_JSON).content(content))
                .andExpect(status().is2xxSuccessful());
        productTestService.assertStockQuantity(7L, milkId);
        productTestService.assertStockQuantity(6L, breadId);
    }

    @Test
    @DirtiesContext
    void testCreateOutOfStock() throws Exception {
        final String content = String.format("""
                {
                    "items": [
                        {
                            "productId": %d,
                            "quantity": 11
                        },
                        {
                            "productId": %d,
                            "quantity": 12
                        }
                    ]
                }
                """, milkId, breadId);
        mockMvc.perform(post("/orders").contentType(MediaType.APPLICATION_JSON).content(content))
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.outOfStockItems[0].productId", is(milkId), Long.class))
                .andExpect(jsonPath("$.outOfStockItems[0].quantityMissing", is(1L), Long.class))
                .andExpect(jsonPath("$.outOfStockItems[1].productId", is(breadId), Long.class))
                .andExpect(jsonPath("$.outOfStockItems[1].quantityMissing", is(2L), Long.class));
        productTestService.assertStockQuantity(10L, milkId);
        productTestService.assertStockQuantity(10L, breadId);
    }

    @Test
    @DirtiesContext
    void testUpdate() throws Exception {
        final OrderItem milkItem = new OrderItem(milkId, 2L);
        final OrderItem breadItem = new OrderItem(breadId, 1L);
        final Long orderId = orderTestService.createOrder(OrderState.CREATED, OffsetDateTime.now(),
                List.of(milkItem, breadItem));
        final String content = """
                {
                    "state": "CANCELLED"
                }
                """;
        mockMvc.perform(patch("/orders/{id}", orderId).contentType(MediaType.APPLICATION_JSON).content(content))
                .andExpect(status().is2xxSuccessful());
        orderTestService.assertOrderState(OrderState.CANCELLED, orderId);
        productTestService.assertStockQuantity(12L, milkId);
        productTestService.assertStockQuantity(11L, breadId);
    }
}
