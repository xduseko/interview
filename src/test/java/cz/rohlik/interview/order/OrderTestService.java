package cz.rohlik.interview.order;

import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Service
public class OrderTestService {

    private final OrderRepository orderRepository;

    public OrderTestService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Long createOrder(OrderState state, OffsetDateTime timestamp, List<OrderItem> items) {
        final Order order = orderRepository.save(new Order(state, timestamp, items));
        return order.getId();
    }

    public void assertOrderState(OrderState state, Long orderId) {
        final Order order = orderRepository.findById(orderId).get();
        assertEquals(state, order.getState());
    }
}
