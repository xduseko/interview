package cz.rohlik.interview.product;

import cz.rohlik.interview.IntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@IntegrationTest
@AutoConfigureTestDatabase
@AutoConfigureMockMvc
class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductTestService productTestService;

    @Test
    @DirtiesContext
    void testCreate() throws Exception {
        final String content = """
                {
                    "name": "Milk",
                    "unitPrice": 20,
                    "stockQuantity": 10
                }
                """;
        mockMvc.perform(post("/products").contentType(MediaType.APPLICATION_JSON).content(content))
                .andExpect(status().is2xxSuccessful());
        final List<Product> products = productRepository.findAll();
        assertEquals(1, products.size());
        final Product milk = products.get(0);
        assertEquals("Milk", milk.getName());
        assertEquals(20L, milk.getUnitPrice());
        assertEquals(10L, milk.getStockQuantity());
    }

    @Test
    @DirtiesContext
    void testUpdate() throws Exception {
        final Long id = productTestService.createProduct("Milk", 20L, 5L, false);
        final String content = """
                {
                    "name": "Bread",
                    "unitPrice": 40,
                    "stockQuantity": 10
                }
                """;
        mockMvc.perform(put("/products/{id}", id).contentType(MediaType.APPLICATION_JSON).content(content))
                .andExpect(status().is2xxSuccessful());
        final Product product = productRepository.findById(id).get();
        assertEquals("Bread", product.getName());
        assertEquals(40L, product.getUnitPrice());
        assertEquals(10L, product.getStockQuantity());
    }

    @Test
    @DirtiesContext
    void testDelete() throws Exception {
        final Long id = productTestService.createProduct("Milk", 20L, 5L, false);
        mockMvc.perform(delete("/products/{id}", id)).andExpect(status().is2xxSuccessful());
        final Product deletedMilk = productRepository.findById(id).get();
        assertEquals(true, deletedMilk.getDeleted());
    }

    @Test
    @DirtiesContext
    void testList() throws Exception {
        productTestService.createProduct("Milk", 20L, 5L, false);
        productTestService.createProduct("Bread", 30L, 6L, true);
        mockMvc.perform(get("/products").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[*]", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is("Milk")))
                .andExpect(jsonPath("$[0].unitPrice", is(20)))
                .andExpect(jsonPath("$[0].stockQuantity", is(5)));
    }
}
