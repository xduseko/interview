package cz.rohlik.interview.product;

import org.springframework.stereotype.Service;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Service
public class ProductTestService {

    private final ProductRepository productRepository;

    public ProductTestService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Long createProduct(String name, Long unitPrice, Long stockQuantity) {
        return createProduct(name, unitPrice, stockQuantity, false);
    }

    public Long createProduct(String name, Long unitPrice, Long stockQuantity, Boolean deleted) {
        final Product product = productRepository.save(new Product(name, unitPrice, stockQuantity, deleted));
        return product.getId();
    }

    public void assertStockQuantity(Long quantity, Long productId) {
        final Product product = productRepository.findById(productId).get();
        assertEquals(quantity, product.getStockQuantity());
    }
}
